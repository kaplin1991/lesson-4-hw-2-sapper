'use strict'
/*
<!--Задание 2-->
     <!--
         Написать реализацию игры "Сапер".
 
         Технические требования:
 
         Нарисовать на экране поле 8*8 (можно использовать таблицу или набор блоков).
         Сгенерировать на поле случайным образом 10 мин. Пользователь не видит где они находятся.
         Клик левой кнопкой по ячейке поля "открывает" ее содержимое пользователю.
 
         Если в данной ячейке находится мина, игрок проиграл. В таком случае показать 
         все остальные мины на поле. Другие действия стают недоступны, можно только 
         начать новую игру.
         Если мины нет, показать цифру - сколько мин находится рядом с этой ячейкой.
         Если ячейка пустая (рядом с ней нет ни одной мины) - необходимо открыть все 
         соседние ячейки с цифрами.
 
 
         Клик правой кнопки мыши устанавливает или снимает с "закрытой" ячейки флажок мины.
         После первого хода над полем должна появляться кнопка "Начать игру заново",  
         которая будет обнулять предыдущий результат прохождения и заново инициализировать поле.
         Над полем должно показываться количество расставленных флажков, и общее количество 
         мин (например 7 / 10).
 
 
         Не обязательное задание продвинутой сложности:
 
         При двойном клике на ячейку с цифрой - если вокруг нее установлено такое же 
         количество флажков, что указано на цифре этой ячейки, открывать все соседние ячейки.
 
         Добавить возможность пользователю самостоятельно указывать размер поля. 
         Количество мин на поле можно считать по формуле Количество мин = количество ячеек / 6.
     -->
*/

// Константы для стилей
//-----------------------------------------------------------------
const   tdWidthAndHeight = 35,
        tdMargin = 5,
        tdDefaultColor = 'silver',
        bombColor = 'red';

let     isGameStart = false,
        firstMove = false,
        mineFound = false,
        flagPut = 0;

const buttonInint = document.getElementById('buttonInit');
 
// Начинаем игру
//-----------------------------------------------------------------
buttonInint.addEventListener('click', () => {
    if(isGameStart){
        return
    }

    //Проверяем ввел ли пользователь число
    //-----------------------------------------------------------------
    const numOfRows = +sapperInit.value;  
    if( isNaN(numOfRows) || numOfRows == '' ){
        alert('Введите число!');
        return;
    }
    const numOfFields = numOfRows * numOfRows; // число ячеёк

    isGameStart = true; // Блокируем возможность начать игру

    document.querySelector('.flagWrapp').style.display = 'flex'; // число флажков

    // Создаем поле и задаем стили
    //-----------------------------------------------------------------
    const create = function(tag){
        return document.createElement(tag);
    }

    const section = document.getElementById('sapper-section'); 
    let tbody = create('tbody');
    section.append(tbody);

    for(let i = 1; i <= numOfRows; i++){
        let tr = create('tr');
        tbody.append(tr);

        for(let i = 1; i <= numOfRows; i++){
            let td = create('td');
            tr.append(td);
        } 
    } 
    
    section.style.cssText = ` 
        width: ${ numOfRows *  (tdWidthAndHeight + tdMargin*2) }px;
        height: ${ numOfRows * (tdWidthAndHeight + tdMargin*2) }px; 
    `;
    
    // Генерируем мины
    //-----------------------------------------------------------------
    const   [...tdArr] = document.getElementsByTagName('TD'),   
            [...trArr] = document.getElementsByTagName('TR'), 
            numOfMines = Math.floor(numOfFields / 6),
            minesArr = [];

    while(minesArr.length < numOfMines){
        let randomNum = Math.floor( Math.random() *( tdArr.length - 0) + 0 );

        if ( !minesArr.includes(randomNum ) ){
            minesArr.push(randomNum);
        } 
    }
    
    // Ограничение на количество флагов
    //-------------------------------
    const   flagPutDiv = document.querySelector('.flagPut'),
            flagAllDiv = document.querySelector('.flagAll'),
            flagAll = numOfMines;
    
    flagPutDiv.innerText = '0';
    flagAllDiv.innerText = `/${flagAll}`;


    // Меняем клас для ячейки с миной
    //-----------------------------------------------------------------
    minesArr.forEach( (item) => {
        tdArr[item].classList.remove('0');
        tdArr[item].classList.add('1');
    });

    // Узнаем количество мин вокруг каждой клетки
    //-----------------------------------------------------------------
    const checkTr = function (indexClickedTr, indexInTr){  
        if(indexClickedTr < 0 || indexInTr <0 || indexClickedTr > numOfRows-1|| indexInTr > numOfRows-1){
            return  
        }else{
            const [...arrayOfTdCheck] = trArr[indexClickedTr].children;
            const checkTd = arrayOfTdCheck[indexInTr];
            const indexCheckedTd = tdArr.indexOf(checkTd);   
            return indexCheckedTd;   
        }
    } 

    tdArr.forEach(tdItem => {

        const nearestTr = tdItem.closest('tr'), // parent tr of clicked td 
            [...nearestTrCildrens] = nearestTr.children, // array of td siblings 
            indexInTr = nearestTrCildrens.indexOf(tdItem), // index in clicked tr
            indexClickedTr = trArr.indexOf(nearestTr); // index of clicked tr  
    
        const checkTheMine = [
            checkTr(indexClickedTr, indexInTr-1), 
            checkTr(indexClickedTr, indexInTr+1), 
            checkTr(indexClickedTr-1, indexInTr), 
            checkTr(indexClickedTr-1, indexInTr-1), 
            checkTr(indexClickedTr-1, indexInTr+1), 
            checkTr(indexClickedTr+1, indexInTr), 
            checkTr(indexClickedTr+1, indexInTr-1), 
            checkTr(indexClickedTr+1, indexInTr+1)                    
        ]
    
        let minesQuantity = 0;
        
        checkTheMine.forEach((el) => {    
            if( tdArr[el] == undefined ){
                return ;
            }else {
                if(tdArr[el].classList.contains('1')){
                    return minesQuantity++;
                }
            }
            
        });
     
        tdItem.setAttribute('data-minesAround', minesQuantity);
    });
 

    tdArr.forEach(tdItem => {
        tdItem.style.cssText = `
            width: ${tdWidthAndHeight}px;
            height: ${tdWidthAndHeight}px; 
            //margin: ${tdMargin}px;
            border:1px solid white;
            background-color: ${tdDefaultColor};
        `;

        if( !tdItem.classList.contains('1') ){
            tdItem.classList.add('0'); 
        }

        //Вешаем событие на каждую ячейку. 
        //-----------------------------------------------------------------
        tdItem.addEventListener('click', function ( ) { 
            // Нельзя нажать если установлен флаг
            //----------------------- 
            if( tdItem.classList.contains('flag') ){
                return;
            } 

            // Если нажали на мину
            //-----------------------
            if( mineFound ){
                return;
            }  

            // Создаем кнопку "Начать игру заново"
            //-----------------------------------------------------------------
                const buttonsSection = document.getElementById('button_indicator-section');
                const resetButton = create('button');
                resetButton.setAttribute('type','button');
                resetButton.innerText = 'Начать игру заново';
            if(firstMove == false){
                buttonsSection.append(resetButton);
                firstMove = true;
            }

            // Нажатие кнопки 'Начать игру заново'. Возвращаем все переменные на начальное значение.
            //-----------------------------------------------------------------
            resetButton.addEventListener('click', function(){
                isGameStart = false; 
                firstMove = false;
                sapperInit.value = ''; 
                tbody.remove();
                resetButton.remove();
                flagPut = 0;
                flagPutDiv.innerText = flagPut;
                document.querySelector('.flagWrapp').style.display = 'none'; // число флажков
                mineFound = false;
            });            

            //При нажатии на ячейку проверяем наличие мины
            //-------------------------------------------------------------------
            let mineCheck = tdItem.classList; 
            
            if( mineCheck.contains('1') ){ 
                minesArr.forEach( (item) => {
                    tdArr[item].style.backgroundColor = bombColor;
                });
                mineFound = true;
            }else{
                const showMines = tdItem.getAttribute('data-minesAround');
                tdItem.innerText = showMines;

                if( showMines == 0){ 

                    //Если 0 мин, открываем соседние клетки
                    //-------------------------------------
                    const nearestTr = tdItem.closest('tr'), // parent tr of clicked td 
                        [...nearestTrCildrens] = nearestTr.children, // array of td siblings 
                        indexInTr = nearestTrCildrens.indexOf(tdItem), // index in clicked tr
                        indexClickedTr = trArr.indexOf(nearestTr); // index of clicked tr  
                
                    const checkTheMine = [
                        checkTr(indexClickedTr, indexInTr-1), 
                        checkTr(indexClickedTr, indexInTr+1), 
                        checkTr(indexClickedTr-1, indexInTr), 
                        checkTr(indexClickedTr-1, indexInTr-1), 
                        checkTr(indexClickedTr-1, indexInTr+1), 
                        checkTr(indexClickedTr+1, indexInTr), 
                        checkTr(indexClickedTr+1, indexInTr-1), 
                        checkTr(indexClickedTr+1, indexInTr+1)                    
                    ];

                    checkTheMine.forEach((el) => {  
                        if(el == undefined){
                            return
                        }else{ 
                            if( tdArr[el].classList.contains('flag') ){
                                tdArr[el].classList.remove('flag');
                                flagPut--;
                                flagPutDiv.innerText = flagPut;
                            }
                            const showMines = tdArr[el].getAttribute('data-minesAround'); 
                            tdArr[el].innerText = showMines; 
                        } 
                    });                

                }
            } 
                        
        }); //tdItem.addEventListener END
        
        // Устанавливает флаг при нажатии правой клавиши и считаем их
        //------------------------------
        tdItem.addEventListener('contextmenu', function(event){
            event.preventDefault();
    
            if( this.innerText ){ 
                return;
            }
            if( !mineFound ){
                if( this.classList.contains('flag') ){
                    this.classList.toggle("flag");
                    flagPut--;
                    flagPutDiv.innerText = flagPut;
                }else{
                    if( flagPut == flagAll ){
                        return
                    }else{
                        this.classList.toggle("flag");
                        flagPut++;
                        flagPutDiv.innerText = flagPut;
                    }
                }
            }
        });

    });  //tdArr.forEach END

    

});